#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>
//#include <opencv2/objdetect.hpp>
#include <opencv2/opencv.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QImage Mat2QImage(cv::Mat const& src);
    cv::Mat inputMap;
    QImage resutlImage;

private slots:
    void openButtonClicked();
    void showResult();
    void saveButtonClicked();
};

#endif // MAINWINDOW_H
