#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include "qdebug.h"
#include <QFileDialog>
#include <QMessageBox>
using namespace cv;
using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("salt and pepper filter");
    connect(ui->openButton,SIGNAL(clicked()),this,SLOT(openButtonClicked()));
    connect(ui->contrastCheckBox,SIGNAL(stateChanged(int)),this,SLOT(showResult()));
    connect(ui->saveButton,SIGNAL(clicked()),this,SLOT(saveButtonClicked()));
    ui->saveButton->setVisible(false);
    ui->contrastCheckBox->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openButtonClicked()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,
                "Open file",
                "./",
                tr("images (*.png *.jpg *.jpeg)" )
                );
    QImage sourceImage(fileName);
    if(sourceImage.isNull())
    {
        QMessageBox messageBox;
        messageBox.critical(this,"Error","Could not open or find the image");
        return;
    }
    ui->inputLabel->setPixmap(QPixmap::fromImage(sourceImage).scaled(ui->inputLabel->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
    inputMap = imread(fileName.toStdString(), IMREAD_COLOR);
    showResult();
    ui->saveButton->setVisible(true);
    ui->contrastCheckBox->setVisible(true);
}

void MainWindow::showResult()
{
    Mat resultMat;
    medianBlur(inputMap,resultMat,5);
    resutlImage = Mat2QImage(resultMat);
    if(ui->contrastCheckBox->isChecked())
    {
        Mat canny_output;
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        int thresh = 100;
        Canny( resultMat, canny_output, thresh, thresh*2, 3 );
        findContours( canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

        Scalar color = Scalar( 0,0,255 );
        Mat contourMat = resultMat;
        for( int i = 0; i< contours.size(); i++ )
        {
            drawContours( contourMat, contours, i, color, 2);
        }
        QImage counturImage = Mat2QImage(contourMat);
        ui->resultLabel->setPixmap(QPixmap::fromImage(counturImage).scaled(ui->resultLabel->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
    }
    else
    {
        ui->resultLabel->setPixmap(QPixmap::fromImage(resutlImage).scaled(ui->resultLabel->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
    }
}

QImage MainWindow::Mat2QImage(Mat const& src)
{
     cv::Mat temp;
     cvtColor(src, temp,CV_BGR2RGB);
     QImage dest((const uchar *) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
     dest.bits();
     return dest;
}

void MainWindow::saveButtonClicked()
{
    QStringList extList;
    extList<<"png"<<"jpg"<<"jpeg";
    QString fileName = QFileDialog::getSaveFileName(this,"Save file","./", tr("images (*.png *.jpg *.jpeg)" ));
    QFileInfo fi(fileName);
    QString ext = fi.completeSuffix();
    if(extList.contains(ext))
        resutlImage.save(fileName);
    else
    {
        QMessageBox messageBox;
        messageBox.critical(this,"Error","Entered extension is not image (* .png * .jpg * .jpeg)");
    }

}
