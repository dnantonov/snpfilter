#-------------------------------------------------
#
# Project created by QtCreator 2019-07-29T16:12:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = snpfilter
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.h

FORMS += \
        mainwindow.ui
INCLUDEPATH += /home/dmitry/opencv3b/include/opencv/
INCLUDEPATH += /home/dmitry/opencv3b/include/opencv2/

unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_calib3d
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_core
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_dnn
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_features2d
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_flann
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_highgui
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_imgcodecs
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_imgproc
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_ml
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_objdetect
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_photo
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_stitching
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_video
unix:!macx: LIBS += -L$$PWD/../../opencv3b/lib/ -lopencv_videoio
